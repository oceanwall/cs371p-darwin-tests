#!/bin/bash
set -e

# usage: using a reference copy of RunCollatz in this directory,
# the name of the owner of the inbound merge request to test their outputs
# e.g. ./collatz-merge-check.sh theBrianCui
if [[ ! -e "./RunDarwin" ]]
then echo "./RunDarwin executable not found"
     exit 1
fi
chmod +x ./RunDarwin

# BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
REMOTE_URL=$(git config --get remote.origin.url)
echo "REMOTE_URL: $REMOTE_URL"
REPO_NAME=${REMOTE_URL:56}

GITLAB_ID=${REPO_NAME:0:$(( $(expr index "$REPO_NAME" "/") - 1 ))}
echo "Gitlab ID: $GITLAB_ID"

RIN="${GITLAB_ID}-RunDarwin.in"
ROUT="${GITLAB_ID}-RunDarwin.out"
TOUT="${GITLAB_ID}-RunDarwin.tmp"

echo "Checking file names..."
if [[ ! -e "$RIN" || ! -e "$ROUT" ]]
then
    echo "FAIL: File names incorrect (could not find $RIN and $ROUT)"
    exit 1
fi

echo "Checking changes..."
git fetch https://gitlab.com/gpdowning/cs371p-darwin-tests.git master
git branch class FETCH_HEAD
MERGE_CHANGES_FULL=$(git diff --shortstat class...)
MERGE_CHANGES=${MERGE_CHANGES_FULL:0:16}
if [[ "$MERGE_CHANGES" != " 2 files changed" ]];
then
    echo "FAIL: Commit contains extraneous changes: $MERGE_CHANGES"
    exit 1
fi

echo "Checking line count..."
COUNT=$(wc -l < "$RIN")
echo "Line Count: $COUNT"
if (( "$COUNT" < 100 || "$COUNT" > 100000 ));
then
    echo "FAIL: Acceptance test line count not within [100, 100000]"
    exit 1
fi

# CHECKTESTDATA=$(checktestdata ../TestDarwin.ctd "$RIN" 2>&1 || echo $?)
# echo "$CHECKTESTDATA"
# if [[ "$CHECKTESTDATA" != "testdata ok!" ]]
# then
#     echo "FAIL: checktestdata failed to validate $RIN"
#     git checkout master
#     exit 1
# fi

echo "Checking test count..."
FIRSTLINE=$(head -1 "$RIN")
echo "Test Count: $FIRSTLINE"
if (( "$FIRSTLINE" < 10 || "$FIRSTLINE" > 20 ))
then
    echo "FAIL: Acceptance test quantity not within [10, 20]"
    exit 1
fi

echo "Running tests, please wait..."
./RunDarwin < "$RIN" > "$TOUT"
echo "Checking diff..."

DIFF=$(diff "$ROUT" "$TOUT" 2>&1 | cat -vet || echo $?)
# rm "$TOUT"
if [[ "$DIFF" != "" ]]
then
   echo "$DIFF"
   echo "FAIL: Reference solution output did not match expected output"
   exit 1
fi

echo "PASS"
exit 0
